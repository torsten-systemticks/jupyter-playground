package de.systemticks.jupyter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import de.systemticks.jupyter.model.Notebook;
import de.systemticks.jupyter.service.NotebookService;

@RestController
public class NotebookController {

  @Autowired
  private NotebookService service;

  @GetMapping("/notebooks")
  ResponseEntity<List<Notebook>> all() {
    List<Notebook> notebooks = service.getAllNotebooks();
    return new ResponseEntity<>(notebooks, HttpStatus.OK);
  }
    
}
