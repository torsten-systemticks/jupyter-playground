package de.systemticks.jupyter.model;

public class Notebook {

    private String path;    

    public Notebook(String path) {
        this.path = path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    
}
