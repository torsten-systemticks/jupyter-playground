package de.systemticks.jupyter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JupyterDemoBackend {

	public static void main(String[] args) {
		SpringApplication.run(JupyterDemoBackend.class, args);
	}

}
