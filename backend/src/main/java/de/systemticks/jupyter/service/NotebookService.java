package de.systemticks.jupyter.service;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import de.systemticks.jupyter.model.Notebook;

@Service
public class NotebookService {

    private static final String NOTEBOOK_PATH = "../notebooks";
    private static final String NOTEBOOK_EXTENSION = ".ipynb";

    public List<Notebook> getAllNotebooks() {

        List<Notebook> notebooks = Collections.emptyList();

        File folder = new File(NOTEBOOK_PATH);
        if (folder.exists() && folder.isDirectory()) {
            
            File[] files = folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(NOTEBOOK_EXTENSION);
                }
            });

            notebooks = Arrays.stream(files).map( f -> new Notebook(f.getName())).collect(Collectors.toList());

        }

        return notebooks;
    }

}
