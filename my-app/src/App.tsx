import React from 'react';
import logo from './logo.svg';
import './App.css';

const url = process.env.REACT_APP_VOILA_URL

function App() {
  return (
      <div>
        <h1>Showing a jupyter Notebook in a react SPA</h1>
        <VoilaIframe />
      </div>
    );
}

function VoilaIframe() {
  return (
    <div>
    <div>Voila running at {url}</div> 
    <div style={{ display: 'flex' }}>
      <iframe
        src={url}
        style={{ flex: 1, height: '800px' }}
      />
      <iframe
        src={url}
        style={{ flex:1 , height: '800px' }}
      />
    </div>
    </div>
  );
}

export default App;
